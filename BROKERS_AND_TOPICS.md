# Brokers & Topics

Example: Topic-A with 3 partitions

If you have decided for three partitions and three Brokers you will automatically 
get the topic distributed into three partitions across the cluster!

![](BrokersAndTopics1.png?raw=true)

Example: Topic-B with 2 partitions
--> Broker 3 has NO data from Topic-B

![](BrokersAndTopics2.png?raw=true)
