# Producers

* Producers will write data to topics
* Producers automatically knows to which Broker and Partition to write to
    * Just connect and start producing - rest is magic
* In case of Broker failures, Producers will automatically recover

![](Producers1.png?raw=true)
Some sort of Round-Robin distribution

## Quality of Service
Three send "modes":
![](QoSProducers.png?raw=true)
Note: default is acks=1

## Message Keys
* Producers can choose to send a key with the message (String, number, etc)
* if key = null, data is sent round robin (broker 101, 102, 103)
* if a key is provided - all messages with that key will always go to the same partition!
* A key is basically something you send if you need message ordering for a specific field (truck_id, message_type)
* it is based on key hashing... - we guarantee that a specific key always goes to the same partition

![](ProducersAndKeys.png?raw=true)


