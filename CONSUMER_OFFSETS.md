# Consumer Offsets

* Kafka stores the offsett at which a consumer **group** has been reading
* The offsets are committed "live" in a Kafka topic! 
    * named __consumer_offsets
    * The action of comittings the offsets into Kafka is essentially writing into the topic __consumer_offsets
* Purpose is to let a consumer pick up from where it left if something goes wrong

## Delivery semantics for consumers

* Consumers choose when they commit their offsets!
* Three deliver sematics


