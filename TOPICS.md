# Topics

Kafka topics are:

* A particular stream of data
    * A little bit like a database table (without constraints)
    * You can have as many topics as you want
    * A topic is identified by its name

* Topics are split in partitions
    * Each partition is ordered
    * Each message within each partition gets an incremental id (offset)
    * Offsets only have a meaning within a partition
    * Order is ONLY guaranteed within a partition (not across)
    * Data is kept only for a limited time (default is one week)
        * Offsets will keep incrementing - they never go back!
    * Once the data has been written to a partition it cannot change - immutable
    * Data is assigned randomly to a partition unless a key is provided

![](topics-parts.png?raw=true)

