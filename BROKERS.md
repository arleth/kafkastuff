# Brokers

* A Kafka cluster is composed of multiple Brokers (servers)
    * Together they act as a Kafka Cluster
* A Broker has its own local storage
* Each Broker is identified by an ID (Integer)
* Each Broker contains certain topic partitions - it is distributed
* After connecting to any broker (called the bootstrap broker) you are connected to the entire cluster!
    * A good starting # is 3
  
![](KafkaBrokers.png?raw=true)
