# Consumers

* Consumers read data from a topic (identified by a name)
* Consumers know which broker to read from
* In case of brokers failure, consumers know how to recover
* Data is read in order **within each partition**
  
![](ConsumerReading.png?raw=true)

## Consumer Groups

* Consumers read data in Consumer Groups
* Each consumer within a group reads from exclusive partitions
* If you have more consumers than partitions - some will be inactive
* If you have more consumers than partitions, some consumers will be inactive
![](ConsumerGroups01.png?raw=true)
**Note**: Consumers will automatically use a GroupCoordinator and a ConsumerCoordinator to assign a consumer to a partition


