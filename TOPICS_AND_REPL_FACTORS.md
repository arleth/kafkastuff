# Topics contd.
## Topics and Replication Factors

* Topics should have a replication factor > 1 (usually 2 or 3)
* This way - if the broker is down another server can serve
Example: Topic-A with 2 partitions and replication factor of 2
  --> replication factor of 2 means that we have two copies of each
  
![](TopicReplicationFactorOf2.png?raw=true)

## Partition Leader

* At any point in time only ONE broker can be a leader for a given partition
* Only that leader can receive and serve data for a partition
* The other brokers will just be synchronizing the data.
* Hence, each partition has one leader and multiple ISR (In-Sync-Replicas)
  
![](TopicLeaders.png?raw=true)

